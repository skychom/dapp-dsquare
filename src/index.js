/* global web3 */

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Web3 from 'web3'

function initApp(web3) {
  ReactDOM.render(<App web3={web3} />, document.getElementById('root'))
  registerServiceWorker()
}

function getProvider(useInjected) {
  var provider = "ws://localhost:7545" // "http://localhost:8545"

  if (useInjected && typeof web3 !== 'undefined' && typeof web3.currentProvider !== 'undefined') {
    provider = web3.currentProvider
  }
  return provider
}

window.addEventListener('load', () => {
  initApp(new Web3(getProvider(false))) // altere para true caso queira usar o MetaMask ou DApp Browsers
})
