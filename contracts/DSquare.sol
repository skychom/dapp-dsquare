pragma solidity ^0.4.18;

contract DSquare {

    mapping (address => uint[]) private checkins;
    uint public lastCheckin;

    event Checkin(address account, uint now);

    function checkin() public {
        uint[] storage userCheckins = checkins[msg.sender];
        if (userCheckins.length > 0) {
          require(now - userCheckins[userCheckins.length - 1] > 1 minutes);
        }

        lastCheckin = now;
        userCheckins.push(now);

        Checkin(msg.sender, now);
    }

    function getCheckins() public view returns (uint[]) {
        return checkins[msg.sender];
    }
}
