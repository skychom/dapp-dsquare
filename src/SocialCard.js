import React from 'react'
import { Button, Card, Icon, Image } from 'semantic-ui-react'
import moment from 'moment'

const SocialCard = ({name, lastCheckin, checkin}) => (
  <Card>
    <Image src='/images/matthew.png' />
    <Card.Content>
      <Card.Header>
        { name }
      </Card.Header>
      <Card.Meta>
        <span className='date'>
          { lastCheckin === undefined ?
            "Didn't check in yet" :
            `Last checkin in: ${moment(lastCheckin).fromNow()}`
          }

        </span>
      </Card.Meta>
      <Card.Description>
        Matthew is a musician living in Nashville.
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
      <Button basic color='green' onClick={() => checkin()}>Check in</Button>
    </Card.Content>
  </Card>
)

export default SocialCard
