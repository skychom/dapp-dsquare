import React, { Component } from 'react'
import SocialCard from './SocialCard'
import './App.css'

class App extends Component {

  constructor() {
    super()
    this.state = { inited: false }
  }

  loadContractJSON() {
    return new Promise((resolve, reject) => {
      fetch('contracts/DSquare.json').then(response => {
        response.json().then(json => resolve(json))
      })
    })
  }

  setContract(json) {
    return new Promise((resolve, reject) => {
      const web3 = this.props.web3

      const abi = json.abi
      const address = json.networks['5777'].address

      const contract = new web3.eth.Contract(abi, address)
      this.subscribeEvents(contract)

      this.setState({ contract }, () => resolve())
    })
  }

  subscribeEvents(contract) {
    contract.events.Checkin()
    .on('data', (event) => this.handleCheckin(event))
  }

  checkin() {
    const contract = this.state.contract

    contract.methods.checkin().send({
      from: this.state.account
    })
    .on('error', (error) => {
      console.log(`Can't checkin.`)
    })
  }

  updateLastCheckin() {
    const contract = this.state.contract
    if (!contract) return

    console.log(`updateLastCheckin: ${this.state.account}`)

    contract.methods.getCheckins().call({
      from: this.state.account
    }).then((result) => {
      console.log(result)
      if (result.length > 0) {
        this.setState({ lastCheckin: result[result.length - 1] * 1000 })
      }
    })
    .catch((error) => {
      console.log(`Can't get last checkin.`)
    })
  }

  init() {
    this.loadContractJSON()
    .then(json => this.setContract(json))
    .then(() => {
       this.props.web3.eth.getAccounts().then((accounts) => {
        console.log(`Accounts: ${accounts}`)
        this.setState({ account: accounts[0], inited: true })
      })
    })
  }

  handleCheckin(event) {
    const { account, now } = event.returnValues
    if (account === this.state.account) {
      this.setState({ lastCheckin: now * 1000 })
    }
    console.log(`Last Checkin: ${account} checked in ${now}`)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.inited && !prevState.inited) {
      this.updateLastCheckin()
    }
    if (this.state.lastCheckin !== undefined && prevState.lastCheckin === undefined) {
      setInterval(() => this.forceUpdate(), 1000)
    }
  }

  componentDidMount() {
    this.init()
  }

  render() {
    const contractAvailable = this.state.contract !== undefined
    return (
      <div className="App">
        <SocialCard name="Pedro" lastCheckin={this.state.lastCheckin} checkin={() => this.checkin()} />
      </div>
    );
  }
}

export default App;
